<!-- MarkdownTOC -->

- 分布式系统 & Git
- 简明入门Git
    - 创建仓库
    - 克隆现有仓库
    - 文件状态转移
    - 文件重命名、忽略和删除
    - 提交历史
    - 伟大的撤销操作
        - 重置的奥秘
    - 远程仓库的配置
    - 版本控制之打标签
    - 命令别名
- Git精髓之分支模型
    - 分支简介
    - 分支进阶
    - 分支管理

<!-- /MarkdownTOC -->

# 分布式系统 & Git
>git是开源，免费的分布式版本控制系统。git的字面意思是愚蠢或不开心的人。

# 简明入门Git

## 创建仓库

- **项目的文件夹**目录下输入
- **git init**
- 生成 .git 空文件夹
    + 包含仓库必须的骨干文件

## 克隆现有仓库

> 如果你想要获得一份已经存在的Git仓库的拷贝
> 这时就要用到 git clone 命令 ——《Pro Git》

- 当仓库是 Public，公开仓库，那么无需验证即可clone
- 从本地的某个文件夹克隆： **git clone 文件夹地址**
- 从服务器克隆：**git clone 地址:path/to/repository**
    + SSH 方式：**git clone git@gitlab.com:Sucran/git-practice.git**
    + Https方式：**git clone https://gitlab.com/Sucran/git-practice.git**

> 如果你想要在克隆远程康库的时候，自定义本地仓库的名字

- **git clone 地址:path/to/repository 新名称**
- 本地仓库名字就会为新名称

## 文件状态转移
> 请记住，你工作目录下的每一个文件都不外乎这两种状态：

>**已跟踪和未跟踪**

> 已跟踪的文件是指那些被纳入了版本控制的文件，在**上一次快照**中有他们的记录

> 他们的状态可能处于**被修改，以修改，或已放入暂存区**这三种状态。

![状态转移](lifecycle.png "LiftCycle")

- 提交后，暂存区文件返回为 未修改状态 
- 添加未跟踪文件，会直接放入暂存区
- 添加已跟踪文件，也会放入暂存区
- 跟踪的未修改文件，可以变成未跟踪的文件，gitignore

> **git status**

- 查看哪些文件处于什么状态
- 根据文件状态转移的图，status 就有如下几种情况
    + **change to commit + new file** 【1】
        * 表示文件在**暂存区**，**Stage**
    + **change not staged for commit  + modified** 【2】
        * 表示文件在**暂存区**，**Modified**
        * **需要再次添加到暂存区**，否则提交的是上一个版本
        * 状态2，都是被提交过了的，已跟踪的文件
    + **nothing to commit, wordking directory clean** 【3】
        * 表示文件在**工作区**，**Unmodified** 
    + **untracked files** 【4】
        * 表示文件**Untracked**

> git status -s

- 状态简览（以下 - 表示空格）
- A 表示状态【1】
    + 新添加的
- -M 出现在右边，状态【2】
- M- 出现左边，状态【1】
    + 等同于 A -> (-M) + add = M-
- MM 状态【2】，在工作区被修改，加入到暂存区后，又被修改
    + 所以在暂存区和工作区都有记录
    + 等同于A -> -M -> M- -> (M-)+(-M) = MM
- ?? 表示未跟踪



> **git add**

- 将文件添加到暂存区，适用状态有 【2】 【4】

> **git diff**

- 当文件加入到暂存区后，就有了一个拷贝
    + diff 就可以查看**修改后**和**拷贝**的差别
- 加入暂存区再提交到工作区，就有了两个相同的拷贝
    + 修改后，再提交到暂存区，就可以查看**修改后的**，和**工作区的拷贝**的差别，*暂存区的被覆盖*

## 文件重命名、忽略和删除

## 提交历史

- 查看历史，是为了想要回到过去
```
git log --oneline
    13ee939 分支进阶理解完成
    7734c64 git 进阶看到一半
    ca763dc git 进阶看到一半
    55f8157 分支简介理解完成
```
- 一旦回到了过去，未来是否还有痕迹？ Git 告诉你有！
    
```
git reflog --oneline
    13ee939 HEAD@{0}: commit: 分支进阶理解完成
    7734c64 HEAD@{1}: commit: git 进阶看到一半
    ca763dc HEAD@{2}: commit: git 进阶看到一半
```
- 当回到过去，还可以用reflog，查看痕迹，回到未来
- 至于为什么要这样？就是为了后悔！
- 为了伟大的撤销操作！

## 伟大的撤销操作

- **git add 少添加文件了 或者 git commit 写错提交信息了**
- 想后悔，那么就
> git commit --amend
- 可以提交后，直接覆盖掉之前的提交对象，不生成新对象

#### 重置的奥秘

- 

## 远程仓库的配置

## 版本控制之打标签

## 命令别名

# Git精髓之分支模型

## 分支简介
- 提交时，Git 保存 提交对象 (Commit Object)
- **提交对象**包含 (快 作 交 父) - (快做交付)
    + 一个暂存指向内容快照的指针
    + 作者的姓名和邮箱
    + 提交输入的信息
    + 指向它父对象的指针
        * 第一次提交，没有父对象
        * 多个分支合并产生的提交对象，有多个父对象
- 添加三个文件到暂存区，然后提交，
```
 git add a b c
 git commit -m "test"
```
- 这样Git仓库中应该有**5个对象**
    + 三个blob对象（保存文件快照）
    + 一个树对象（记录目录结构和blob对象索引）
    + 一个提交对象（包含树对象指针和所有提交信息）

![提交树](commit-tree.png "Commit Tree")

- 提交了多次之后，每次产生的提交对象都会有指向上次提交对象（即父对象）的指针

![父对象](commits-parents.png "Commit Parents")

- git init 默认创造一个 master 分支
- Git 的分支，本质就是指向提交对象的可变指针。

![提交后的master分支](master-init.png "Master init")

- 创建一个 testing 分支, 就是创建一个可以移动的新指针

> git branch testing 

- Git 固定有一个 HEAD 特殊指针，指向当前所在的本地分支
- 所以一开始，HEAD应该 指向 master分支

- 可以使用指令查看各个分支当前所指的提交对象，要加参数 --decorate
```
 git log --oneline --decorate
 结果：
    f30ab (HEAD, master, testing) add feature #32 - ability to add new
    34ac2 fixed bug #1328 - stack overflow under certain conditions
    98ca9 initial commit of my project
```
- 可以从日志内容看出，HEAD、master、testing三者指向同一个提交对象
- 只做添加分支的操作，不会出现分叉

- 当需要切换到testing 分支时
> git checkout testing

- HEAD是指向当前本地分支，所以HEAD会指向testing

![切换分支](head-testing.png "Head To Testing")

- 如果现在再提交一次，会出现一个新的提交对象，那么就有下图的情况

![分支超前](advance-testing.png "Adnvance Testing")

> git checkout master 

- 切换回主分支，这样 HEAD 会再指向 master分支
    + 这条命令，还讲工作目录恢复成master所指的快照内容
- 然后再做修改，再提交，就会出现分叉

![分叉](advance-master.png "Adnvance Master")

- 这时也能够用到 git log 查看分支
```
git log --oneline --decorate --graph --all
结果：
    * c2b9e (HEAD, master) made other changes
    | * 87ab2 (testing) made a change
    |/
    * f30ab add feature #32 - ability to add new formats to the
    * 34ac2 fixed bug #1328 - stack overflow under certain conditions
    * 98ca9 initial commit of my project
```
- 每当出现一个分支，就讲所指对象校验和写入一个文件，校验和一般只有41个字节
- 所以分支的创建和销毁都异常高效

## 分支进阶

- 需求1：
    + 原本有一个主分支
    + 要解决一个问题issue53需要新建了一个分支

![新建分支](basic-branching-1.png "Basic Branching")

```
    git checkout -b iss53
等同于
    git branch iss53
    git checkout iss53
```
![分支结果](basic-branching-2.png "Basic Branching")

- 在iss53上有所进展，保存进度后即

![分支超前](basic-branching-3.png "Basic Branching")

- 需求1后遇到需求2：
    + 需要解决主分支上的一个bug
    + 解决后在合并到主分支
```
    (当前处于 iss53分支)
    git checkout master
    git checkout -b hotfix
```
- **切换到主分支master之前，先注意工作区和暂存区有没有modified的文件**
- **最好在切换分支前，保持一个干净的状态 clean**
- 解决之后，再提交，hotfix就超前master了

![主分支再分支](basic-branching-4.png "Basic Branching")

- 处理完bug之后，master分支理应要合并hotfix分支

```
    (当前处于hotfix分支)
    git checkout master
    git merge hotfix
        Updating f42c576..3a0874c
        Fast-forward
        index.html | 2 ++
        1 file changed, 2 insertions(+)
```
- 此时，显示Fast-forward，之前提到分支实质是一个指向提交对象的指针
- 而当没有需要解决分歧时，指针直接向前移动即可，所以Fast-forword

![主分支更新](basic-branching-5.png "Basic Branching")

- 这时就直接删除hotfix即可，因为master的工作区已经更新到修改bug之后了
```
    git branch -d hotfix
```

- 然后再继续解决iss53的分支，需要切换回去，当前在master分支
- 有所进展之后再提交

![分支更新](basic-branching-6.png "Basic Branching")

- **这时，iss53是不包含hotfix更改的**
- 可以直接先把分支合并 到master
- 也可以等之后iss53做完后在合并到master

- **做完iss53要合并回master**

```
    (此时分支是iss53)
    git checkout master
    git merge iss53
```
- 由于iss53的直接祖先不是master，所以Git需要参考其公共祖先

![分支合并](basic-merging-1.png "Basic Merging")

- 做一个三方参考合并，然后自动创建一个提交并指向它，即一次合并提交
- 注意是三方参考，那么新创建的提交对象，就有两个父对象

![分支合并结果](basic-merging-2.png "Basic Merging")

- **遭遇冲突的合并**
```
    （在master分支上)
    git merge iss53
        Auto-merging index.html
        CONFLICT (content): Merge conflict in index.html
        Automatic merge failed; fix conflicts and then commit the result.
```
- 出现冲突错误时，有显示冲突文件，这是可以再使用 git status 查看哪些文件
```
    git status
        On branch master
        You have unmerged paths.
          (fix conflicts and run "git commit")

        Unmerged paths:
          (use "git add <file>..." to mark resolution)

            both modified:      index.html

        no changes added to commit (use "git add" and/or "git commit -a")
```
- **任何因包含合并冲突而有待解决德尔文件，都会以未合并状态标识**
- ```<<<<<<<, =======, >>>>>>>```这三个标识，会分割开不同版本区别
```
    <<<<<<< HEAD:index.html
    <div id="footer">contact : email.support@github.com</div>
    =======
    <div id="footer">
     please contact us at support@github.com
    </div>
    >>>>>>> iss53:index.html
``` 
- ```<<<<<<<``` 和 ```=======``` 指当前master分支中HEAD工作区所存储的
- ```>>>>>>>``` 和 ```=======``` 指iss53分支中暂存的部分
- **选择留下一个部分**，同时再删除标识即可

- 最后添加 git add ， 然后 git status 确认所有合并冲突都已被解决
- 最后再git commit 完成合并然后提交，结果可能如下：
```
    Merge branch 'iss53'

        Conflicts:
            index.html
        #
        # It looks like you may be committing a merge.
        # If this is not correct, please remove the file
        #   .git/MERGE_HEAD
        # and try again.


        # Please enter the commit message for your changes. Lines starting
        # with '#' will be ignored, and an empty message aborts the commit.
        # On branch master
        # All conflicts fixed but you are still merging.
        #
        # Changes to be committed:
        #   modified:   index.html
        #
```
## 分支管理

```
    git branch
        iss53
        * master
        testing
``` 
- 显示的是，所有当前的分支，即当前存在的所有有指向的指针

```
    git branch -v
        iss53   93b412c fix javascript issue
        * master  7a98805 Merge branch 'iss53'
        testing 782fd34 add scott to the author list in the readmes
```
- 显示每个分支，并显示其指向的提交对象所包含的提交信息

- 可以用 --merged 和 --no-merged 查看所有包含的合并和未合并的分支

```
    git branch --merged
        iss53
        *master
```
- 表示iss53已经合并到当前的master分支

```
    git branch --no-merged
        testing
```

- 对于**还没有合并的分支**

```
    git branch -d testing
        error: The branch 'testing' is not fully merged.
        If you are sure you want to delete it, run 'git branch -D testing'.
        (git branch -D testing)可以删除掉
```





















